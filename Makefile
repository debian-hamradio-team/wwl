# September 2007
# Diane Bruce <va3db@amsat.org>

PREFIX?=	/usr/local
MANPREFIX?=	/usr/local/man
MAN1PREFIX?=	${MANPREFIX}/man1
LN?=		ln
INSTALL?=	install
GZIP?=		gzip
CP?=		cp
RM?=		rm
MKDIR?=		mkdir
TAR?=		tar
RELEASE=	wwl+db-1.3

all: wwl locator

wwl:	wwl.c 
	$(CC) $(CFLAGS) -o wwl wwl.c -lm

locator: wwl
	${RM} -f locator
	${LN} -s wwl locator

install:	all
	${RM} -f ${PREFIX}/bin/locator
	${RM} -f ${PREFIX}/bin/wwl
	${INSTALL}	wwl ${PREFIX}/bin/wwl
	${LN} -s ${PREFIX}/bin/wwl ${PREFIX}/bin/locator
	${INSTALL} wwl.1 ${MAN1PREFIX}/wwl.1
	${GZIP} -f ${MAN1PREFIX}/wwl.1
	${LN} -s ${MAN1PREFIX}/wwl.1.gz ${MAN1PREFIX}/locator.1.gz
	
deinstall:
	${RM} -f ${PREFIX}/bin/wwl
	${RM} -f ${PREFIX}/bin/locator
	${RM} -f ${MAN1PREFIX}/wwl.1.gz
	${RM} -f ${MAN1PREFIX}/locator.1.gz

release:
	@${RM} -rf ${RELEASE}
	@${MKDIR} ${RELEASE}
	@${CP} Makefile ${RELEASE}
	@${CP} wwl.c ${RELEASE}
	@${CP} wwl.1 ${RELEASE}
	@${CP} INSTALL ${RELEASE}
	${TAR} cvfz ${RELEASE}.tar.gz ${RELEASE}
	@${RM} -rf ${RELEASE}

clean:
	rm -f wwl *~ wwl locator *.o
	rm -rf ${RELEASE}.tar.gz ${RELEASE}
